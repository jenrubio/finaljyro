package deloitte.academy.lesson01.entity;

/**
 * Clase abstracta para la creaci�n de art�culos. 
 * Contiene 4 m�todos abstractos: read, update, delete y create.
 * @author jenrubio
 * @version 1.0
 * @since 2020-03-10
 * @link https://www.tutorialspoint.com/java/java_documentation.htm
 */
public abstract class Articulos {
	private String codigo;
	private String nombre;
	private double precio;
	private int cantidad;
	
	/** Constructor por defecto */
	public Articulos() {
		// TODO Auto-generated constructor stub
	}
	
	/** Constructor */
	public Articulos(String codigo, String nombre, double precio, int cantidad) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.precio = precio;
		this.cantidad = cantidad;
	}

	/** Getters y setters */
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	
	/** M�todos abstractos */
	public abstract String read(Articulos articulo);
	
	public abstract void update(Articulos articulo);
	
	public abstract void delete(Articulos articulo);

	public abstract void create(Articulos articulo);
}
