package deloitte.academy.lesson01.machine;

import deloitte.academy.lesson01.entity.Articulos;
import deloitte.academy.lesson01.run.EjecutarMaquina;

/**
 * Clase que se extiende de la superclass Articulos.
 * En esta se encuentran los m�todos: Read, Update,
 * Delete y Create.
 * @author jenrubio
 * @version 1.0
 * @since 2020-03-10
 */
public class ArtMaquina extends Articulos{
	
	/** Constructores */
	public ArtMaquina() {
	}
	
	public ArtMaquina(String codigo, String nombre, double precio, int cantidad) {
		super(codigo, nombre, precio, cantidad);
	}

	/**
	 * M�todo que permite verificar que el art�culo
	 * tiene inventario. 
	 * De ser as�: 
	 * @param articulo: objeto de la clase Articulos.
	 * @see ("B3", "Halls", 10.10, 10)
	 * @return salida: OK(el producto tiene inventario), 
	 * KO: El producto no tiene inventario
	 */
	@Override
	public String read(Articulos articulo) {
		String salida = "KO";
		for(Articulos art : EjecutarMaquina.listaArticulos) {
			if(art.getCodigo() == articulo.getCodigo()) {
				if(art.getCantidad() != 0) {
					salida = "OK";
					break;
				}
			}
		}
		
		return salida;
	}

	/**
	 * M�todo para actualizar un art�culo ya existente.
	 * NO se puede modificar el c�digo del art�culo, �nicamente
	 * se puede modificar el nombre, cantidad y precio. 
	 * @param articulo: objeto de la clase Articulos.
	 * @code arti.setNombre(articulo.getNombre());  
	 * arti.setCantidad(articulo.getCantidad());  
	 * arti.setPrecio(articulo.getPrecio());
	 */
	@Override
	public void update(Articulos articulo) {
		for(Articulos arti : EjecutarMaquina.listaArticulos) {
			if(articulo.getCodigo() == arti.getCodigo()) {
				arti.setNombre(articulo.getNombre());
				arti.setCantidad(articulo.getCantidad());
				arti.setPrecio(articulo.getPrecio());
				break;
			}
		}

	}

	/**
	 * M�todo que permite ir descontando 1 a 1 la cantidad
	 * de el art�culo que se indica. 
	 * @param articulo: objeto de la clase Articulos.
	 * @code articulo.setCantidad(articulo.getCantidad()-1);
	 */
	@Override
	public void delete(Articulos articulo) {
		articulo.setCantidad(articulo.getCantidad()-1);

	}

	/**
	 * M�todo para agregar un art�culo nuevo a la lista
	 * de Art�culos que se encuentra en la clase EjecutarMaquina.
	 * @param articulo: objeto de la clase Articulos.
	 * @code EjecutarMaquina.listaArticulos.add(articulo);
	 */
	@Override
	public void create(Articulos articulo) {
		EjecutarMaquina.listaArticulos.add(articulo);
	}

}
