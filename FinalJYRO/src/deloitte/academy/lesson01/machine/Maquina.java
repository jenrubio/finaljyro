package deloitte.academy.lesson01.machine;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Articulos;
import deloitte.academy.lesson01.run.EjecutarMaquina;

/**
 * Clase que manda llamar ciertos m�todos de la clase ArtMaquina para poder
 * dispensar, actualizar o agregar un art�culo. 
 * @author jenrubio
 * @version 1.0
 * @since 2020-03-10
 * 
 */
public class Maquina {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	ArtMaquina art = new ArtMaquina();
	
	
	/**
	 * M�todo que permite dispensar un art�culo cuando se selecciona
	 * en el men� principal el usuario cliente. Verifica que el
	 * art�culo tenga inventario y de ser as�, lo despacha. De lo contrario
	 * env�a un log de que el objeto est� agotado. 
	 * @param articulo: Objeto de la clase Articulo.
	 * @see delete() y read()
	 * @exception log --> Level.SEVERE
	 * @throws ex: Env�a un mensaje de error indicando que se vuelva a intentar.
	 */
	public void dispensarArticulo(Articulos articulo) {
		try {
			String resultado = art.read(articulo);
			if(resultado == "OK") {
				for(Articulos art : EjecutarMaquina.listaArticulos) {
					if(art.getCodigo() == articulo.getCodigo()) {
						articulo.delete(art);
						LOGGER.info("\n\nDispensando art�culo....");
						EjecutarMaquina.totalVentas += art.getPrecio();
						EjecutarMaquina.cantidadVentas++;
						break;
					}
				}
			}
			else {
				LOGGER.info("Articulo sin inventario");
			}
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
	}
	
	/**
	 * M�todo que permite actualizar ciertos datos de un art�culo. 
	 * Manda a llamar el m�todo update de ArtMaquina para poder realizar
	 * dicha acci�n. 
	 * @param articulo: Objeto de la clase Articulo. 
	 * @see update()
	 * @exception log --> Level.SEVERE
	 * @throws ex: Env�a un mensaje de error indicando que se vuelva a intentar. 
	 */
	public void actualizarArticulo(Articulos articulo){
		try {
			art.update(articulo);
			LOGGER.info("Articulo actualizado!!");
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
	}
	
	/**
	 * M�todo void que permite agregar un art�culo �nicamente si no existe en
	 * el inventario. 
	 * Manda llamar el m�todo create de la clase ArtMaquina. 
	 * @param articulo Objeto de la clase Articulo.
	 * @see create()
	 * @exception log --> Level.SEVERE
	 * @throws ex: Env�a un mensaje de error indicando que se vuelva a intentar.
	 */
	public void agregarArticulo(Articulos articulo) {
		
		try {
			if(!EjecutarMaquina.listaArticulos.contains(articulo)) {
				art.create(articulo);
				LOGGER.info("Se agreg� correctamente el art�culo");
			}else {
				LOGGER.info("El art�culo ya existe.");
			}
		}catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
	}

	
}
