package deloitte.academy.lesson01.machine;

/**
 * Enum que permite desplegar ciertos mensajes en el men� principal.
 * @author jenrubio
 * @version 1.0
 * @since 2020-03-10
 *
 */
public enum Mensajes {
	BIENVENIDA("      BIENVENIDO HUMANO     "), MENU("Selecciona tu tipo de usuario"), USUARIO("1. Cliente\n2. Administrador"), 
	ADMIN("1.Editar art�culo\n2.A�adir nuevo art�culo\n3.Imprimir inventario");
	
	
	private Mensajes() {
		// TODO Auto-generated constructor stub
	}
	
	public String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	private Mensajes(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
}
