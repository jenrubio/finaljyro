package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Articulos;
import deloitte.academy.lesson01.machine.ArtMaquina;
import deloitte.academy.lesson01.machine.Maquina;
import deloitte.academy.lesson01.machine.Mensajes;

/**
 * Clase Main del proyecto de Maquina Expendedora. 
 * Permite mandar a ejecutar los m�todos de la clase M�quina. 
 * @author jenrubio
 * @version 1.0
 * @since 2020-03-10
 *
 */
public class EjecutarMaquina {
	public static final List<Articulos> listaArticulos = new ArrayList<Articulos>();
	public static double totalVentas = 0;
	public static int cantidadVentas = 0;
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());
	
	public static void main(String[] args) {
		/** Declaraci�n de variables */
		int opcion2 = 0;
		String codigoComprar = "";
		
		/** Objeto de la clase Maquina() */
		Maquina maquinaDisp = new Maquina();
		
		
		/** Declaraci�n de los art�culos */
		ArtMaquina artA1 = new ArtMaquina("A1", "Chocolate", 10.50, 10);
		listaArticulos.add(artA1);
		ArtMaquina artA2 = new ArtMaquina("A2", "Doritos", 15.50, 4);
		listaArticulos.add(artA2);
		ArtMaquina artA3 = new ArtMaquina("A3", "Coca", 22.50, 2);
		listaArticulos.add(artA3);
		ArtMaquina artA4 = new ArtMaquina("A4", "Gomitas", 8.75, 6);
		listaArticulos.add(artA4);
		ArtMaquina artA5 = new ArtMaquina("A5", "Chips", 30.00, 10);
		listaArticulos.add(artA5);
		ArtMaquina artA6 = new ArtMaquina("A6", "Jugo", 15.00, 2);
		listaArticulos.add(artA6);
		ArtMaquina artB1 = new ArtMaquina("B1", "Galletas", 10.00, 3);
		listaArticulos.add(artB1);
		ArtMaquina artB2 = new ArtMaquina("B2", "Canelitas", 120.00, 6);
		listaArticulos.add(artB2);
		ArtMaquina artB3 = new ArtMaquina("B3", "Halls", 10.10, 10);
		listaArticulos.add(artB3);
		ArtMaquina artB4 = new ArtMaquina("B4", "Tarta", 3.14, 10);
		listaArticulos.add(artB4);
		ArtMaquina artB5 = new ArtMaquina("B5", "Sabritas", 15.55, 0);
		listaArticulos.add(artB5);
		ArtMaquina artB6 = new ArtMaquina("B6", "Cheetos", 12.25, 4);
		listaArticulos.add(artB6);
		ArtMaquina artC1 = new ArtMaquina("C1", "Rocaleta", 10.00, 1);
		listaArticulos.add(artC1);
		ArtMaquina artC2 = new ArtMaquina("C2", "Rancherito", 14.75, 6);
		listaArticulos.add(artC2);
		ArtMaquina artC3 = new ArtMaquina("C3", "Ruffles", 13.15, 10);
		listaArticulos.add(artC3);
		ArtMaquina artC4 = new ArtMaquina("C4", "Pizza fr�a", 22.00, 9);
		listaArticulos.add(artC4);
		
		/** Variable tipo Scanner que permite leer el teclado */
		Scanner teclado = new Scanner(System.in);
		
		/** do-while para poder continuar en el men� */
		do {
			/**
			 * Inicia el men�. 
			 * Se utiliza Enums para el mensaje de bienvenida y desplegar mensaje tipo usuarios.
			 * Enums se utilizar� para desplegar el men� del administrador m�s adelante. 
			 */
			System.out.println("\n" + Mensajes.BIENVENIDA.mensaje);
			System.out.println(Mensajes.MENU.mensaje);
			System.out.println(Mensajes.USUARIO.mensaje);
			int usuario = Integer.parseInt(teclado.nextLine());
			
			/**
			 * Opcion 1: Cliente --> Solo puede comprar un producto
			 * Opcion 2: Administrador --> Puede editar o a�adir un art�culo e imprimir
			 * la lista de los art�culos junto con cantidad de ventas y ganancia.
			 */
			boolean opc = (usuario <= 2);
			if(opc) {
				if(usuario == 1) {
					/** For-each para imprimir los art�culos */
					for(Articulos arti : listaArticulos) {
						System.out.println(arti.getCodigo() + " " + arti.getNombre() + " $" + arti.getPrecio());
					}
					
					/** Timer de 1 segundo */
					try {
						Thread.sleep (1000);
					} catch (Exception e) {
						LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", e);
					}
					
					/** Aqu� se ingresa el c�digo del art�culo a compar
					 * >>>>>>>>>>>>>>>>>>>>>C�digo art�culo a comprar<<<<<<<<<<<<<<<<<<<<<<<<< */
					codigoComprar = "A1";
					System.out.println("Ingresa el c�digo del art�culo a comparar: \n" + codigoComprar);
					
					
					/** Permite checar si el art�culo se encuentra en la lista */
					for(Articulos art : listaArticulos) {
						if(art.getCodigo() == codigoComprar) {
							maquinaDisp.dispensarArticulo(art);
							System.out.println("\n\nCantidad de ventas : " + cantidadVentas);
							break;
						}
					}
				}
				else if(usuario == 2) {
					System.out.println("\n\nSelecciona la acci�n a realizar");
					System.out.println(Mensajes.ADMIN.mensaje);
					int opc3 = Integer.parseInt(teclado.nextLine());
					
					/**
					 * Switch que permite seleccionar la opci�n deseada del administrador
					 * Case 1: Editar un art�culo
					 * Case 2: A�adir un nuevo art�culo
					 * Case 3: Imprime la lista de art�culos junto con el total de ventas 
					 * en cantidad y ganancia. 
					 */
					switch (opc3) {
					case 1: {
						/** Articulo a editar */
						ArtMaquina artEditar = new ArtMaquina("C1", "Rocaleta", 5.50, 0);
						maquinaDisp.actualizarArticulo(artEditar);
						
						/** Se imprime la lista para ver los cambios */
						for(Articulos art : listaArticulos) {
							System.out.println(art.getCodigo() + " " + art.getNombre() + " " +
						art.getCantidad() + " $" + art.getPrecio());
						}
						break;
					}
					case 2: {
						/** Articulo a a�adir */
						ArtMaquina artAgregar = new ArtMaquina("C5", "Picafresa", 1.00, 100);
						maquinaDisp.agregarArticulo(artAgregar);
						
						/** Se imprime la lista para ver los cambios */
						for(Articulos art : listaArticulos) {
							System.out.println(art.getCodigo() + " " + art.getNombre() + " " +
						art.getCantidad() + " $" + art.getPrecio());
						}
						break;
					}
					case 3: {
						/** Imprimir lista */
						for(Articulos art : listaArticulos) {
							System.out.println(art.getCodigo() + " " + art.getNombre() + " " +
						art.getCantidad() + " $" + art.getPrecio());
						}
						
						/** Se imprime el total de ventas. */
						System.out.println("\n\nTotal de ventas: $" + totalVentas);
						System.out.println("Cantidad de ventas: " + cantidadVentas);
						break;
					}
					default:
						throw new IllegalArgumentException("Este valor no existe: " + opc3);
					}
				}
			}else {
				System.out.println("Ingresa 1 o 2, dependiendo de la opci�n deseada");
			}
			
			System.out.println("\nDesea hacer alguna otra operacion?");
			System.out.println("1. Si");
			System.out.println("2. No");
			opcion2 = Integer.parseInt(teclado.nextLine());
		}while(opcion2 == 1);
		
		
		
	}

}
